<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ["isi", "user_id"];

    //show post from user
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function username()
    {
        return $this->belongsTo('App\Post', 'post_id');
    }

    public function tags(){
        return $this->belongsToMany('App\Tag', 'post_tags', 'post_id', 'tag_id');
    }

    public function comments() {
        return $this->hasMany('App\Comment');
    }

}
