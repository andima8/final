<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;
use App\User;
use Auth;
use App\Profile;

class ProfileController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $posts = $user->posts;
        $check = $user->profile;
        if($check == null) {
            return redirect('/profile/create');
        } else {
            return view('profile', compact('posts','check'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('createprofile');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'ttl' => 'required',
            'email' => 'required',
            'photo' => 'required|file|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        // menyimpan data file yang diupload ke variabel $file
		$file = $request->file('photo');
		$nama_file = time()."_".$file->getClientOriginalName();
      	// isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'img';
		$file->move($tujuan_upload,$nama_file);

        $query = Profile::create([
            'nama' => $request['nama'],
            'alamat' => $request['alamat'],
            'ttl' => $request['ttl'],
            'email' => $request['email'],
            'user_id' => Auth::user()->id,
            'photo' => $nama_file
        ]);
        return redirect('profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data= User::find($id);
        return view('profile', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = DB::table('profiles')->where('id', $id)->first();
        return view('edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'photo' => 'file|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $file = $request->file('photo');
		$nama_file = time()."_".$file->getClientOriginalName();
      	// isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'img';
		$file->move($tujuan_upload,$nama_file);


        $query = Profile::where('id', $id)->update([
            'nama' => $request['nama'],
            'alamat' => $request['alamat'],
            'ttl' => $request['ttl'],
            'email' => $request['email'],
            'photo' => $nama_file
        ]);    

    return redirect('/profile')->with('success', 'Data berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $postD = Post::find($id);
        //dd($postD); 
        $postD->delete();
        return redirect('/profile')->with('success', 'Post berhasil dihapus');
    }
}
