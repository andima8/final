<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use RealRashid\SweetAlert\Facades\Alert;
use Auth;
use App\Tag;
use App\Comment;
use App\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id){
        
    }

    //Menyimpan Data Postingan
    public function store(Request $request){
        $request->validate([
            'isi' => 'required',
        ]);
        
        $tags_arr = explode(',' , $request["tags"]);
        
        $tag_ids = [];
        foreach($tags_arr as $tag_name){
            $tag = Tag::firstOrCreate(['tag_name'=>$tag_name]);
            $tag_ids[] = $tag->id;
            // $tag = Tag::where("tag_name", $tag_name)->first();
            // if($tag) {
            //     $tag_ids[] = $tag->id;
            // } else {
            //     $new_tag = Tag:: create(["tag_name" => $tag_name]);
            //     $tag_ids[] = $new_tag->id;
            // }
        }

        $user = Auth::user();
        $post = Post::create([
            "isi" => $request["isi"],
            "gambar" => $request["gambar"],
            "user_id" => $user->id
        ]);

        $post->tags()->sync($tag_ids);
        $user = Auth::user();
        $user->posts()->save($post);


        Alert::success('Berhasil', 'Berhasil membuat post');
        return redirect('/home');//->with('success','Postingan berhasil disimpan');
    }


    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $show = Post::all();
        return view('home',compact('show'));
    }

    

}
