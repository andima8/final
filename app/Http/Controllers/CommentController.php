<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Comment;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;
use User;
use App\Post;

class CommentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('comment');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    

    public function store(Request $request,$id){
        $request->validate([
            'isi' => 'required',
        ]);

         $posts = Comment::create([
             "isi" => $request["isi"],
             "post_id" => $id
         ]);

        $postID = Auth::user()->posts->first()->id;
        Alert::success('Berhasil', 'Berhasil membuat comment');
        return redirect()->route('post.comment', ['id' => $postID]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show = Post::find($id);
        $posts = Post::find($id)->comments;
        return view('comment',compact('show','posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $postID = Auth::user()->posts->first()->id;
        Comment::destroy($id);
        return redirect()->route('post.comment', ['id' => $postID]);
    }
}
