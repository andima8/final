@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit Profile') }}</div>

                <div class="card-body">
                    <form method="POST" action="/profile/{{Auth::id()}}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Nama') }}</label>
                            <div class="col-md-6"> 
                                <input id="nama" type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{!empty(Auth::user()->profile->nama) ? Auth::user()->profile->nama:"" }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="alamat" class="col-md-4 col-form-label text-md-right">{{ __('Alamat') }}</label>
                            <div class="col-md-6">
                                <input id="alamat" type="text" class="form-control @error('alamat') is-invalid @enderror" name="alamat" value="{{!empty(Auth::user()->profile->alamat) ? Auth::user()->profile->alamat:"" }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ttl" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Lahir') }}</label>
                            <div class="col-md-6">
                                <input id="ttl" type="date" class="form-control @error('ttl') is-invalid @enderror" name="ttl" value="{{!empty(Auth::user()->profile->ttl) ? Auth::user()->profile->ttl:"" }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>
                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{!empty(Auth::user()->profile->email) ? Auth::user()->profile->email:"" }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="photo" class="col-md-4 col-form-label text-md-right">{{ __('Photo') }}</label>
                            <input type="file" name="photo" id="photo">
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update Profile') }}
                                </button>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
