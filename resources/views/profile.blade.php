@extends('layouts.app')
<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}</a>
        <td><a href="/home">Home</a></td>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
@section('content')
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<div class="container">
    <div class="row profile">
		<div class="col-md-3">
			<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<img src={{url('/img/'.$check->photo)}} class="img-responsive" alt="{{url('/img/noimage.jpg')}}">
				</div>
				<!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                        @empty (Auth::user()->profile->nama)
                        <div class="row justify-content-center"><a href="/profile/create" class="btn btn-primary btn-sm">Create New Profile</a></div>
                        @endempty
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
						{{!empty(Auth::user()->profile->nama) ? Auth::user()->profile->nama:"Your Name" }}
                    </div>
					<div class="profile-usertitle-job">
						{{!empty(Auth::user()->profile->ttl) ? Auth::user()->profile->ttl:"Date of Birth"}}
					</div>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
				<div class="profile-userbuttons">
					<button type="button" class="btn btn-success btn-sm">Follow</button>
					<button type="button" class="btn btn-danger btn-sm">Message</button>
				</div>
				<!-- END SIDEBAR BUTTONS -->
				<!-- SIDEBAR MENU -->
				<div class="profile-usermenu">
					<ul class="nav">
						<li class="active">
							<a href="#">
							<i class="glyphicon glyphicon-home"></i>
							{{!empty(Auth::user()->profile->alamat) ? Auth::user()->profile->alamat:"City"}} </a>
						</li>
						
						<li>
							<a href="#" target="_blank">
							<i class="glyphicon glyphicon-ok"></i>
							{{!empty(Auth::user()->profile->email) ? Auth::user()->profile->email:"Email"}} </a>
						</li>
						
                        <li>
							<a href="/profile/{{Auth::id()}}/edit">
							<i class="glyphicon glyphicon-user"></i>
							Edit/Create Profile</a>
						</li>
					</ul>
				</div>
				<!-- END MENU -->
			</div>
		</div>
		<div class="col-md-9">
            <div class="profile-content">
                <div class="row justify-content-center">
                    @forelse($posts as $key => $post)
                    <div class="card mb-4 mt-4 col-md-8">
                        <div class="card-body">
                            {{-- //tulisan pada tampilan postingan --}}
                            <p class="card-text">{{Auth::user()->name}}</p>
                            <p class="card-text">{{$post->isi}}</p>
                            <tr>
                                <td><a href="#" class="btn btn-default btn-sm">Edit Post</a></li>
                                <td>
                                    <form action="/profile/{{$post->id}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" value="delete" class="btn btn-danger mt-2 btn-sm">
                                    </form>
                                </td>
                            </tr>
                 
                        </div>
                        <div class="card-footer text-muted">
                            Tags:
                            @forelse ($post->tags as $tag)
                            <button class="btn btn-primary btn-sm">{{ $tag->tag_name }}</button> 
                            @empty
                            No tags       
                            @endforelse                            
                        </div>
                        </div>
                      </div>
                      @empty
                      <tr>
                        <td colspan="4" align="center">No Data</td>
                      </tr>
                @endforelse
                </div>
            </div>
		</div>
	</div>
</div>
@endsection