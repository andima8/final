@extends('layouts.app')
<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
  <div class="container">
          <a class="navbar-brand" href="{{ url('/') }}">
              {{ config('app.name', 'Laravel') }}</a>
      <td><a href="/home">Home</a></td>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Left Side Of Navbar -->
          <ul class="navbar-nav mr-auto">

          </ul>

          <!-- Right Side Of Navbar -->
          <ul class="navbar-nav ml-auto">
              <!-- Authentication Links -->
              @guest
                  <li class="nav-item">
                      <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                  </li>
                  @if (Route::has('register'))
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                      </li>
                  @endif
              @else
                  <li class="nav-item dropdown">
                      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                          {{ Auth::user()->name }} <span class="caret"></span>
                      </a>

                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="{{ route('logout') }}"
                             onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                              {{ __('Logout') }}
                          </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>
                      </div>
                  </li>
              @endguest
          </ul>
      </div>
  </div>
</nav>
@section('content')
<div class="row justify-content-center">
    <div class="card mb-4 mt-4 col-md-8">
        <div class="card-body">
            {{-- //tulisan pada tampilan postingan --}}
            <p class="card-text">{{$show->user->name}}</p>
            <p class="card-text">{{$show-> isi}}</p>
        </div>
        <div class="card-footer text-muted col-md-12">
          Tags:
          @forelse ($show->tags as $tag)
          <button class="btn btn-primary btn-sm">{{ $tag->tag_name }}</button> 
          @empty
          No tags       
          @endforelse
          <br>
          <br>
          <form action="/home/comment/{{$show->id}}" method="POST">
            @csrf
            <input type="text" class="isi col-9" id="isi" name="isi" placeholder="Add Comment">
            <br>
            <button type="submit" class="btn btn-primary btn-sm mt-2">Add Comment</button>
          </form>     
          </div>
          @forelse ($posts as $item => $data)
          <div class="card mb-2 mt-8 col-md-12">
           <div class="card-body">
              <p class="card-text">{{$data->isi}}</p>
              <form action="/home/comment/{{$data->id}}" method="POST">
                @csrf
                @method('DELETE')
                <input type="submit" value="delete" class="btn btn-danger mt-2 btn-sm">
            </form>
            </div>
          </div>
          @empty
              
          @endforelse
      </div>
</div>
</div>
@endsection
@if(session('success'))
@push('scripts')
<script>
  Swal.fire({
    title: 'Berhasil!',
    text:  'Comment Berhasil Dibuat',
    icon:  'success',
    confirmButtonText: 'Cool'
  })
</script>
 @endpush
 {{ session('success')}}
 @endif