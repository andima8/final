@extends('layouts.app')

<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
  <div class="container">
          <a class="navbar-brand" href="{{ url('/') }}">
              {{ config('app.name', 'Laravel') }}</a>
      <td><a href="/profile">Profile</a></td>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Left Side Of Navbar -->
          <ul class="navbar-nav mr-auto">

          </ul>

          <!-- Right Side Of Navbar -->
          <ul class="navbar-nav ml-auto">
              <!-- Authentication Links -->
              @guest
                  <li class="nav-item">
                      <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                  </li>
                  @if (Route::has('register'))
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                      </li>
                  @endif
              @else
                  <li class="nav-item dropdown">
                      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                          {{ Auth::user()->name }} <span class="caret"></span>
                      </a>

                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="{{ route('logout') }}"
                             onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                              {{ __('Logout') }}
                          </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>
                      </div>
                  </li>
              @endguest
          </ul>
      </div>
  </div>
</nav>

@section('content')
{{-- Membuat Header Postingan --}}
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                Hello, {{Auth::user()->name}}
                </div>
                <form role="form" action="/home" method="POST">
                    @csrf
                      <div class="card-body">
                        <div class="form-group">
                          <textarea class="form-control" name="isi" id="isi" 
                          value="{{ old('isi','') }}" placeholder="What's New?" 
                          cols="15" rows="5"></textarea>
                          <label for="tags" class="mt-2">Tags</label>
                          <input type="text" class="tags col-12" id="tags" name="tags" placeholder="Pisahkan dengan koma. Contoh: Baru, Lama">
                        </div>
                          @error('isi')
                          <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                      </div>
                      <!-- /.card-body -->
      
                      <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Post</button>
                      </div>
                    </form>
            </div>
        </div>
    </div>

    {{-- Membuat Postingan user --}}
    
    <div class="row justify-content-center">
        @forelse($show as $key => $post)
        <div class="card mb-4 mt-4 col-md-8">
            <div class="card-body">
                {{-- //tulisan pada tampilan postingan --}}
               <p class="card-text">{{$post->user->name}}</p>
                <p class="card-text">{{$post->isi}}</p>
            </div>
            <div class="card-footer text-muted col-md-12">
              Tags:
              @forelse ($post->tags as $tag)
              <button class="btn btn-primary btn-sm">{{ $tag->tag_name }}</button> 
              @empty
              No tags       
              @endforelse
              <br><br>
              <a href="/home/comment/{{$post->id}}" type="submit" class="btn btn-primary btn-sm">Add Comment</a>
              </div>
          </div>
         
          @empty
          <tr>
            <td colspan="4" align="center">No Data</td>
          </tr>

    @endforelse
    </div>
    </div>
   
@endsection
@if(session('success'))
@push('scripts')
<script>
  Swal.fire({
    title: 'Berhasil!',
    text:  'Post Berhasil Dibuat',
    icon:  'success',
    confirmButtonText: 'Cool'
  })
</script>
 @endpush
 {{ session('success')}}
 @endif