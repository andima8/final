<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Home Utama User
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home', 'HomeController@store');

//create comment
Route::post('/home/comment/{id}','CommentController@store')->name('post.comment');
Route::delete('/home/comment/{id}', 'CommentController@destroy');
Route::get('/home/comment/{id}', 'CommentController@show');

//Profile User CRUD
Route::resource('profile', 'ProfileController');
//post crud
Route::resource('post', 'PostController');

